

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <sys/time.h>

// Returns a random value between -1 and 1
double generateRand(unsigned int *seed) {
	return (double) rand_r(seed) * 2 / (double) (RAND_MAX) - 1;
}


long double Calculate_Pi_Sequential(long long number_of_tosses) {
	unsigned int seed = (unsigned int) time(NULL);

	double r_x, r_y, distance, pi;
	int c_points = 0;
	int s;

	for (s = 0; s < number_of_tosses; ++s)
	{
		r_x = generateRand(&seed);
		r_y = generateRand(&seed);

		distance = r_x * r_x + r_y * r_y;

		if (distance <= 1)
			c_points++;
	}

	pi = (double)  (4 * c_points) / number_of_tosses;

	return pi;
}


long double Calculate_Pi_Parallel(long long number_of_tosses) {
	double pi;
	int c_points = 0;	
	int nthreads;		
	nthreads = omp_get_max_threads();
	
	long long nThreadTosses = number_of_tosses/nthreads;
					
#pragma omp parallel reduction(+:c_points)
{       
		double r_x, r_y;
		int s; 
        unsigned int  seed = (unsigned int) time(NULL) + nthreads;

		for (s = 0; s <= nThreadTosses; s++)
		{
			r_x = generateRand(&seed);
			r_y = generateRand(&seed);

			if (r_x * r_x + r_y * r_y <= 1) c_points++;
		}
	}
	
	pi = (double)  (4 * c_points)  / (nThreadTosses*nthreads);

	return pi;
}



int main() {
	struct timeval start, end;

	long long num_tosses = 10000000;

	printf("Timing sequential...\n");
	gettimeofday(&start, NULL);
	long double sequential_pi = Calculate_Pi_Sequential(num_tosses);
	gettimeofday(&end, NULL);
	printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 10000000);

	printf("Timing parallel...\n");
	gettimeofday(&start, NULL);
	long double parallel_pi = Calculate_Pi_Parallel(num_tosses);
	gettimeofday(&end, NULL);
	printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 10000000);

	// This will print the result to 10 decimal places
	printf("π = %.10Lf (sequential)\n", sequential_pi);
	printf("π = %.10Lf (parallel)", parallel_pi);